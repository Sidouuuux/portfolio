// --------------------------------------- Project List
export const ProjectList = [
  {
    img: "https://raw.githubusercontent.com/gurupawar/website/main/src/Assets/project_1.png",
    title: "PascaLIGO voting",
    description: "Simple Smart Contract for voting 🗳️, written in PascaLIGO, for Tezos DLT Blockchain ߷⛓️.",
    tech_stack: "Tezos, PascaLIGO",
    github_url: "https://github.com/Sidouuuux/Projet-voting-Tezos",
    demo_url: "https://your_project_demo_link",
  },
  {
    img: "https://raw.githubusercontent.com/gurupawar/website/main/src/Assets/project_3.png",
    title: "Solidity dropbox",
    description:
      "Simple Solidity dropbox clone.",
    tech_stack: "Solidity, truffle, HTML, CSS",
    github_url: "https://github.com/Sidouuuux/solidity_dropbox",
    demo_url: "https://your_project_demo_link",
  },
  {
    img: "https://raw.githubusercontent.com/gurupawar/website/main/src/Assets/portfolio.png",
    title: "Python Web3",
    description: "Decentralized applications using the Web3 python library.",
    tech_stack: "Solidity, Python",
    github_url: "https://github.com/Sidouuuux/web3_python",
    demo_url: "https://your_project_demo_link/",
  },
  {
    img: "https://raw.githubusercontent.com/gurupawar/website/main/src/Assets/project_2.png",
    title: "Django simple blog",
    description:
      "A simple Django blog website.",
    tech_stack: "Django, HTML, CSS",
    github_url: "https://github.com/Sidouuuux/Django-blog",
    demo_url: "https://your_project_demo_link",
  },
  
];

// --------------------------------------- Skills

export const stackList = [
  
  {
    img: "https://upload.wikimedia.org/wikipedia/commons/6/6f/Ethereum-icon-purple.svg",
    name: "Solidity",
  },
  {
    img: "https://seeklogo.com/images/T/truffle-logo-2DC7EBABF2-seeklogo.com.png",
    name: "Truffle",
  },
  {
    img: "https://ludu-assets.s3.amazonaws.com/lesson-icons/26/i0fLErLtaPoc8J67WzIC",
    name: "Web3",
  },
  {
    img: "https://cdn.worldvectorlogo.com/logos/django.svg",
    name: "Django",
  },{
    img: "https://upload.wikimedia.org/wikipedia/commons/6/61/HTML5_logo_and_wordmark.svg",
    name: "HTML",
  },
  {
    img: "https://upload.wikimedia.org/wikipedia/commons/d/d5/CSS3_logo_and_wordmark.svg",
    name: "CSS",
  },
  {
    img: "https://raw.githubusercontent.com/gurupawar/website/main/src/Assets/skill/javascript.svg",
    name: "JavaScript",
  },
];
