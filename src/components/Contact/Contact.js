import React from "react";
import SocialIcon from "../SocialIcon/SocialIcon";
import { ContactWrapper, Email } from "./ContactElements";
function Contact() {
  return (
    <ContactWrapper id="contact">
      <div className="Container">
        <div className="SectionTitle">Contact me</div>
        <div className="BigCard">
          <Email>
            <span>mirsidahmed@hotmail.fr</span>
            <a
              className="btn PrimaryBtn"
              href="mailto:mirsidahmed@hotmail.fr"
              target="_blank"
              rel="noopener noreferrer"
            >
              Send Mail
            </a>
          </Email>
        </div>
        <SocialIcon />
      </div>
    </ContactWrapper>
  );
}

export default Contact;
