import React, { useState } from "react";
import Dropdown from "../Dropdown/Dropdown";
import Header from "../Header/Header";
import {
  HeroContainer,
  HeroWrapper,
  HeroLeft,
  HeroRight,
  Image,
  ScrollDown,
  ScrollLink,
} from "./HeroElements";
function Hero() {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <main>
      <Dropdown isOpen={isOpen} toggle={toggle} />
      <Header toggle={toggle} />
      <HeroContainer>
        <HeroWrapper>
          <HeroLeft>
            <h1>Hi, I'm Sid-Ahmed MIR</h1>
            <h5>Blockchain Developer</h5>
            <p>
              I'm a blockchain developer and I develop decentralized applications</p>
          </HeroLeft>
          <HeroRight>
            <Image style={{"border-radius": "50% 20% / 10% 40%"}}
              src="https://media-exp1.licdn.com/dms/image/C4E03AQGhsIBv6HPo5A/profile-displayphoto-shrink_400_400/0/1644183942259?e=1649894400&v=beta&t=oigBJK7NU7-xJcewuRVn8OXPCsjE5Ms7wRtjB_oZUG4"
              alt="man-svgrepo"
            />
          </HeroRight>
        </HeroWrapper>
        <ScrollDown to="projects">
          <ScrollLink>
            Scroll down
            <img
              src="https://raw.githubusercontent.com/gurupawar/website/main/src/Assets/scroll-down.svg"
              alt="scroll-down"
            />
          </ScrollLink>
        </ScrollDown>
      </HeroContainer>
    </main>
  );
}

export default Hero;
