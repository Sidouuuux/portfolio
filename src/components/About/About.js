import React from "react";
import { stackList } from "../../data/ProjectData";
import {
  Image,
  Technologies,
  Tech,
  TechImg,
  TechName,
  ContactWrapper,
} from "./AboutElements";
function About() {
  return (
    <ContactWrapper id="about">
      <div className="Container">
        <div className="SectionTitle">About Me</div>
        <div className="BigCard">
          <Image style={{"border-radius": "50% 20% / 10% 40%"}}
            src="https://media-exp1.licdn.com/dms/image/C4E03AQGhsIBv6HPo5A/profile-displayphoto-shrink_400_400/0/1644183942259?e=1649894400&v=beta&t=oigBJK7NU7-xJcewuRVn8OXPCsjE5Ms7wRtjB_oZUG4"
            alt="man-svgrepo"
          />
          <div className="AboutBio">
            Hello! My name is <strong>SId-AHmed MIR</strong>. In Computer Science at the 
            "École Supérieure de Génie Informatique", in Paris in the Blockchain field. 
            I am currently looking for a job as a Solidity developer I choose this field 
            to learn more about an environment that I am passionate about!
            unchanged.
          </div>
          <div className="AboutBio tagline2">
            I have become confident using the following technologies.
          </div>
          <Technologies>
            {stackList.map((stack, index) => (
              <Tech key={index} className="tech">
                <TechImg src={stack.img} alt={stack.name} />
                <TechName>{stack.name}</TechName>
              </Tech>
            ))}
          </Technologies>
        </div>
      </div>
    </ContactWrapper>
  );
}

export default About;
